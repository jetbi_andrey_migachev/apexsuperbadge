@isTest
private class WarehouseCalloutServiceTest {
    @isTest static void testGetCallout(){
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());
        Test.startTest();
        WarehouseCalloutService.runWarehouseEquipmentSync();
        Test.stopTest();
        List<Product2> p = [SELECT Warehouse_SKU__c FROM Product2
                            WHERE Warehouse_SKU__c = '100003'
                            LIMIT 1];
        System.assertEquals(1, p.size());
    }
}