@IsTest
public class TestDataFactory {

    public static List<Case> createTestCases(Integer workPartsQty) {
        Vehicle__c v = new Vehicle__c(Name='Test');
    
        Product2 eq = new Product2(ProductCode = '123',
                              Name = '123',
                              Warehouse_SKU__c = '123',
                              Maintenance_Cycle__c = 69,
                              Replacement_Part__c = true);
        Product2 eq2 = new Product2(ProductCode = '123',
                              Name = '123',
                              Warehouse_SKU__c = '123',
                              Maintenance_Cycle__c = 68,
                              Replacement_Part__c = true);
        insert v;
        insert eq;
        insert eq2;
        List<Case> cases = new List<Case>();
        for(Integer i=0; i<3; i++) {
            Case c = new Case();
            c.Origin = 'Web';
            c.Reason = 'Origin';
            c.Status = 'New';
            c.Vehicle__c = v.Id;
            c.Equipment__c = eq.Id;
            c.Subject='test';
            c.Type='Routine Maintenance';
            cases.add(c);
        }
        insert cases;
        //Case[0] will have 2 related work parts, Case[1] - 1 wp. Other cases - 0.
        Work_Part__c wp1 = new Work_Part__c(Equipment__c = eq.Id,
                                            Quantity__c = 1,
                                            Maintenance_Request__c = cases[0].Id);
        Work_Part__c wp2 = new Work_Part__c(Equipment__c = eq2.Id,
                                            Quantity__c = 1,
                                            Maintenance_Request__c = cases[0].Id);
        Work_Part__c wp3 = new Work_Part__c(Equipment__c = eq.Id,
                                            Quantity__c = 1,
                                            Maintenance_Request__c = cases[1].Id);

        insert wp1;
        insert wp2;
        insert wp3;

        if(workPartsQty == 0) {
            return [SELECT Id,Origin,Reason,AccountId,ContactId,AssetId,
                                            Status,Vehicle__c,Equipment__c,Subject,
                                            Type,Date_Reported__c,Date_Due__c,
                                            (SELECT Id,Equipment__r.Maintenance_Cycle__c 
                                            FROM Work_Parts__r)
                    FROM Case
                    WHERE Id = :cases[2].Id
                    LIMIT 1];
        }
        if(workPartsQty == 1) {
            return [SELECT Id,Origin,Reason,AccountId,ContactId,AssetId,
                                            Status,Vehicle__c,Equipment__c,Subject,
                                            Type,Date_Reported__c,Date_Due__c,
                                            (SELECT Id,Equipment__r.Maintenance_Cycle__c 
                                            FROM Work_Parts__r)
                    FROM Case
                    WHERE Id = :cases[1].Id
                    LIMIT 1];
        }

        // List<Case> casesWithParts = [SELECT Id,Origin,Reason,AccountId,ContactId,AssetId,
        //                                     Status,Vehicle__c,Equipment__c,Subject,
        //                                     Type,Date_Reported__c,Date_Due__c,
        //                                     (SELECT Id,Equipment__r.Maintenance_Cycle__c 
        //                                     FROM Work_Parts__r)
        //                             FROM Case
        //                             WHERE Id IN :casesIds];

        return [SELECT Id,Origin,Reason,AccountId,ContactId,AssetId,
                                            Status,Vehicle__c,Equipment__c,Subject,
                                            Type,Date_Reported__c,Date_Due__c,
                                            (SELECT Id,Equipment__r.Maintenance_Cycle__c 
                                            FROM Work_Parts__r)
                FROM Case
                WHERE Id = :cases[0].Id
                LIMIT 1];
    }

    public static List<Case> createOldCases(){
        List<Case> oldCases = new List<Case>();
        Vehicle__c v = new Vehicle__c(Name='Test');
    
        Product2 eq = new Product2(ProductCode = '123',
                              Name = '123',
                              Warehouse_SKU__c = '123',
                              Maintenance_Cycle__c = 69,
                              Replacement_Part__c = true);
        insert v;
        insert eq;
        for(Integer i = 0; i<25; i++){
            Case c = new Case();
            c.Origin = 'Web';
            c.Reason = 'Origin';
            c.Status = 'New';
            c.Vehicle__c = v.Id;
            c.Equipment__c = eq.Id;
            c.Subject='test';
            c.Type='Routine Maintenance';
            oldCases.add(c);
        }
        for(Integer i = 0; i<25; i++){
            Case c = new Case();
            c.Origin = 'Web';
            c.Reason = 'Origin';
            c.Status = 'New';
            c.Vehicle__c = v.Id;
            c.Equipment__c = eq.Id;
            c.Subject='test';
            c.Type='Repair';
            oldCases.add(c);
        }
        for(Integer i = 0; i<50; i++){
            Case c = new Case();
            c.Origin = 'Web';
            c.Reason = 'Origin';
            c.Status = 'Closed';
            c.Vehicle__c = v.Id;
            c.Equipment__c = eq.Id;
            c.Subject='test';
            c.Type='Other';
            oldCases.add(c);
        }
        insert oldCases;
        return oldCases;
    }

}