@isTest
public class TestMaintenanceRequestHelper {
    
    
    @isTest static void testGetMinMaintenanceCycle() {
        List<Case> case0 = TestDataFactory.createTestCases(0);
        List<Case> case1 = TestDataFactory.createTestCases(1);
        List<Case> case2 = TestDataFactory.createTestCases(2);

        // List<Id> caseIds = new List<Id>();
        // for(Case c : cases) {
        //     caseIds.add(c.Id);
        // }
        // List<Case> casesWithParts = [SELECT Id,Origin,Reason,AccountId,ContactId,AssetId,Vehicle__c,Equipment__c, 
        //                                     (SELECT Id, Equipment__r.Maintenance_Cycle__c FROM Work_Parts__r)
        //                             FROM Case
        //                             WHERE Id IN :caseIds];

        Integer val1 = MaintenanceRequestHelper.getMinMaintenanceCycle(case0[0]);
        Integer val2 = MaintenanceRequestHelper.getMinMaintenanceCycle(case1[0]);
        Integer val3 = MaintenanceRequestHelper.getMinMaintenanceCycle(case2[0]);
        System.debug(val1);
        System.debug(val2);
        System.debug(val3);
        System.assertEquals(0, val1);
        System.assertEquals(69, val2);
        System.assertEquals(68, val3);

    }

    @isTest static void testUpdateWorkOrders() {


        List<Case> oldCases = TestDataFactory.createOldCases();
        List<Case> newCases = TestDataFactory.createOldCases();
        for(Case c: newCases) {
            c.Status = 'Closed';
        }
        update newCases;
        List<Case> legitRequests = MaintenanceRequestHelper.checkRequestsTypeAndStatus(oldCases, newCases);

        System.assertEquals(50, legitRequests.size());

        List<Id> legitIds = new List<Id>();
        for(Case l : legitRequests) {
            legitIds.add(l.Id);
        }
        MaintenanceRequestHelper.updateWorkOrders(oldCases, newCases);
        System.assertEquals(100, [SELECT Subject FROM Case
                                WHERE Subject='not null'].size());
    }
}