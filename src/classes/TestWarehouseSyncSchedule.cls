@isTest
private class TestWarehouseSyncSchedule {

    @isTest static void testScheduler() {
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock());
        WarehouseSyncSchedule scheduler = new WarehouseSyncSchedule();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '0 0 0 ? * *';
        Test.startTest();
        String jobID = System.schedule('Warehouse Sync', sch, scheduler);
        Test.stopTest();
        System.assertNotEquals(null, jobID);
    }
}