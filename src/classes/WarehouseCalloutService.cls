public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
    @future(callout=true)
    public static void runWarehouseEquipmentSync(){
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(WAREHOUSE_URL);
        req.setMethod('GET');
        HttpResponse res = http.send(req);
        
        List<Object> equipment = (List<Object>) JSON.deserializeUntyped(res.getBody());
        List<Product2> equipmentToUpdate = new List<Product2>();
        for(Object obj : equipment) {
            //String temp = JSON.serialize(obj);
            //eqList.add((Map<String, Object>)JSON.deserializeUntyped(temp));
            Map<String, Object> objMap = (Map<String, Object>)obj;
            Product2 e = new Product2();
            e.Maintenance_Cycle__c = Integer.valueOf(objMap.get('maintenanceperiod'));
            e.Name = (String)objMap.get('name');
            e.Cost__c = Integer.valueOf(objMap.get('cost'));
            e.Current_Inventory__c = Integer.valueOf(objMap.get('quantity'));
            e.Lifespan_Months__c = Integer.valueOf(objMap.get('lifespan'));
            e.Replacement_Part__c = (Boolean) objMap.get('replacement');
            e.Warehouse_SKU__c = (String)objMap.get('sku');
            e.ProductCode = (String) objMap.get('_id');
            equipmentToUpdate.add(e);
        }
        upsert equipmentToUpdate;
 
    }

}