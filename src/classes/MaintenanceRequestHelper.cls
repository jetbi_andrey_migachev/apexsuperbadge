public class MaintenanceRequestHelper {
    
    public static void updateWorkOrders(List<Case> oldRequests, List<Case> newRequests){
        
        List<Case> legitRequests = checkRequestsTypeAndStatus(oldRequests, newRequests);
        
        List<Case> requestsToBeCreated = new List<Case>();
        
        for(Case req : legitRequests) {
            Case newReq = new Case();
            newReq.Origin = req.Origin;
            newReq.Reason = req.Reason;
            newReq.AccountId = req.AccountId;
            newReq.ContactId = req.ContactId;
            newReq.AssetId = req.AssetId;
            
            newReq.Status = 'New';
            newReq.Vehicle__c = req.Vehicle__c;
            newReq.Equipment__c = req.Equipment__c;
            newReq.Subject='not null';
            newReq.Type='Routine Maintenance';
            newReq.Date_Reported__c = Date.today();
            newReq.Date_Due__c = Date.today().addDays(getMinMaintenanceCycle(req));
            requestsToBeCreated.add(newReq);
        }
        insert requestsToBeCreated;
    }
    
    public static List<Case> checkRequestsTypeAndStatus(List<Case> oldRequests, List<Case> newRequests) {
        
        List<Case> legitRequests = new List<Case>();
        for(Integer i=0; i<oldRequests.size(); i++) {
            if( ((oldRequests[i].Type=='Repair' || oldRequests[i].Type=='Routine Maintenance') && newRequests[i].Status == 'Closed') && oldRequests[i].Status != 'Closed' ) {
                legitRequests.add(newRequests[i]);
            } 
        }
        return legitRequests;
    }
    
    public static Integer getMinMaintenanceCycle(Case c) {
        
        List<Work_Part__c> wpts = c.Work_Parts__r;
        if(wpts.size() == 0) {
            return 0;
        }
        
        if( wpts.size() == 1) {
            return Integer.valueOf(wpts[0].Equipment__r.Maintenance_Cycle__c);
        }
        
        
        Integer first = Integer.valueOf(wpts[0].Equipment__r.Maintenance_Cycle__c);
        for(Work_Part__c wp : wpts) {
            if (Integer.valueOf(wp.Equipment__r.Maintenance_Cycle__c) < first) {
                first = Integer.valueOf(wp.Equipment__r.Maintenance_Cycle__c);
            }
        }
        return first;
    }
    
}